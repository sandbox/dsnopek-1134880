<?php
/**
 * Filter by analytics period.
 */
class analyticsapi_views_handler_filter_period extends views_handler_filter {
  var $no_single = TRUE;
  var $no_operator = TRUE;

  function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#type' => 'select',
      '#title' => t('Period'),
      '#description' => 'The frequency with which the data is collected (ex. daily, hourly)',
      '#options' => analyticsapi_get_periods(),
      '#default_value' => $this->value,
    );

    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      if (!isset($form_state['input'][$identifier])) {
        $form_state['input'][$identifier] = $this->value;
      }
    }
  }

  function expose_options() {
    parent::expose_options();
    // always isn't optional
    $this->options['expose']['optional'] = FALSE;
  }

  function expose_form_right(&$form, &$form_state) {
    parent::expose_form_right($form, $form_state);
    // it doesn't make sense to make the period optional!
    unset($form['expose']['optional']);
  }

  function query() {
    $this->ensure_my_table();
    $this->query->add_where($this->options['group'], "$this->table_alias.$this->real_field = %d", $this->value);
  }
}

