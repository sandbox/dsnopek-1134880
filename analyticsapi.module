<?php

/**
 * @file
 * Analytics API main module file
 */

/**
 * Implementation of hook_cron().
 */
function analyticsapi_cron() {
  analyticsapi_calculate_aggregates();
}

/**
 * Implementation of hook_views_api().
 */
function analyticsapi_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'analyticsapi'),
  );
}

function analyticsapi_record_event($name, $value) {
  $event = analyticsapi_get_event($name);
  if (is_null($event)) {
    // TODO: error!
    return;
  }

  $table = 'analyticsapi_events_' . $name;

  // enforce the uniqueness constraint
  if (!empty($event['unique'])) {
    $sql = "SELECT event_id FROM {$table} WHERE ";
    $where = array();
    $args = array();
    foreach ($event['unique'] as $field) {
      $where[] = "$field = '%s'";
      $args[] = $value[$field];
    }
    if (db_fetch_object(db_query($sql . implode(" AND ", $where), $args))) {
      // we don't have uniqueness!
      return;
    }
  }

  // write to the table
  $value['event_time'] = time();
  $value = (object)$value;
  drupal_write_record('analyticsapi_events_' . $name, $value);
}

function analyticsapi_get_event($name=NULL, $reset=FALSE) {
  static $events = NULL;

  if (is_null($events) || $reset) {
    $events = module_invoke_all('analyticsapi_events');
  }

  if (is_null($name)) {
    return $events;
  }

  return $events[$name];
}

function analyticsapi_get_slice($name=NULL, $reset=FALSE) {
  static $slices = NULL;

  if (is_null($slices) || $reset) {
    $slices = module_invoke_all('analyticsapi_slices');
  }

  if (is_null($name)) {
    return $slices;
  }

  return $slices[$name];
}

function analyticsapi_get_aggregate($name=NULL, $reset=FALSE) {
  static $aggregates = NULL;

  if (is_null($aggregates) || $reset) {
    $aggregates = module_invoke_all('analyticsapi_aggregates');
  }

  if (is_null($name)) {
    return $aggregates;
  }

  return $aggregates[$name];
}

// TODO: eventually store this in the database!
function analyticsapi_get_periods() {
  return array(
    0 => t('Total'),
    3600 => t('Hourly'),
    84600 => t('Daily'),
    592200 => t('Weekly'),
  );
}

function analyticsapi_calculate_aggregates() {
  $aggregates = analyticsapi_get_aggregate();
  foreach (array_keys(analyticsapi_get_periods()) as $period) {
    foreach (_analyticsapi_generate_datapoints($period) as $datapoint) {
      foreach ($aggregates as $name => $info) {
        _analyticsapi_calculate_aggregate($name, $info, $datapoint);
      }
    }
  }

  // fill in any missing datapoints will zero aggregate values
  _analyticsapi_zero_fill_missing_datapoints();
}

function _analyticsapi_generate_datapoints($period) {
  $datapoints = array();

  // find most recent two datapoints
  $res = db_query("SELECT datapoint_id, time, period FROM {analyticsapi_datapoints} WHERE period = %d ORDER BY time DESC LIMIT 2", $period);
  while ($obj = db_fetch_object($res)) {
    $datapoints[] = $obj;
  }
  if (count($datapoints) == 0) {
    // create the first datapoint in this series
    $datapoints[] = (object)array(
      'time' => $period == 0 ? 0 : variable_get('analyticsapi_start_time', 0),
      'period' => $period,
    );
  }

  if ($period != 0) {
    // we count up from the most recent datapoint until we cross now
    $cur = $datapoints[0]->time + $period;
    $now = time();
    while ($cur < $now) {
      $datapoints[] = (object)array(
        'time' => $cur,
        'period' => $period,
      );
      $cur += $period;
    }
  }

  // TODO: or should we do this after they are successfully generated?
  // now we create all the datapoints that don't already exist in the database
  foreach ($datapoints as &$obj) {
    if (empty($obj->datapoint_id)) {
      drupal_write_record('analyticsapi_datapoints', $obj);
    }
  }

  return $datapoints;
}

function _analyticsapi_calculate_aggregate($name, $info, $datapoint) {
  // get all the colums we are querying for
  $value =  strtoupper($info['function']);
  $value .= '(' . ($info['function'] == 'count' ? '*' : $info['field']) . ')';
  $value .= ' AS ' . $name;

  // add all the slice fields
  $cols = array($value);
  foreach ((array)$info['group by'] as $col => $alias) {
    $cols[] = "$col AS $alias";
  }

  // build the SQL query
  $sql = "SELECT " . implode(", ", $cols);
  $sql .= " FROM {analyticsapi_events_" . $info['event'] . "}";
  $sql .= " WHERE event_time >= %d AND event_time <= %d";
  if (!empty($info['group by'])) {
    $sql .= " GROUP BY " . implode(", ", array_keys($info['group by']));
  }

  // figure out our start and end time
  if ($datapoint->period == 0) {
    $start = variable_get('analyticsapi_start_time', 0);
    $end = time();
  }
  else {
    $start = $datapoint->time;
    $end = $datapoint->time + $datapoint->period;
  }

  // query and store the data
  $res = db_query($sql, $start, $end);
  while ($obj = db_fetch_object($res)) {
    $obj->datapoint_id = $datapoint->datapoint_id;
    _analyticsapi_record_aggregate($info, $obj);
  }
}

function _analyticsapi_record_aggregate($info, $data) {
  $table = 'analyticsapi_slice_' . $info['slice'];
  $where = array('datapoint_id = %d');
  $value = array($data->datapoint_id);
  foreach ((array)$info['group by'] as $col) {
    $where[] = "$col = '%s'";
    $value[] = $data->{$col};
  }

  // check if we are updating or inserting
  $update = array();
  $sql = "SELECT datapoint_id FROM {$table} WHERE " . implode(' AND ', $where);
  if (db_fetch_object(db_query($sql, $value))) {
    $update = array_merge(array('datapoint_id'), array_values($info['group by']));
  }

  // store the actual record
  drupal_write_record($table, $data, $update);
}

function _analyticsapi_zero_fill_missing_datapoints() {
  foreach (analyticsapi_get_slice() as $slice_name => $slice) {
    if (count($slice['fields']) == 0) {
      // filling in zero values doesn't make sense for global slices!
      continue;
    }

    // loop through all the distinct slice ids on this slice
    $table = "analyticsapi_slice_$slice_name";
    $res = db_query("SELECT DISTINCT " . implode(", ", array_keys($slice['fields'])) . " FROM {$table} ");
    while ($ids = db_fetch_array($res)) {
      // insert zero'ed out rows for all the datapoints for this group of slice id's which
      // are currently missing
      $sql  = "INSERT IGNORE INTO {$table} (datapoint_id, " . implode(", ", array_keys($slice['fields'])) . ")";
      $sql .= " SELECT d.datapoint_id, " . implode(", ", array_values($ids));
      $sql .= " FROM {analyticsapi_datapoints} d";

      // execute!
      db_query($sql);
    }
  }
}

