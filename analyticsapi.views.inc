<?php

/**
 * @file
 * For analyticsapi integration with views
 */

/**
 * Implementation of hook_views_data().
 */
function analyticsapi_views_data() {
  $data = array();

  // base table for all aggregate data
  $data['analyticsapi_datapoints'] = array(
    'table' => array(
      'group' => t('Analytics'),
      'base' => array(
        'field' => 'datapoint_id',
        'title' => t('Analytics aggregate data'),
        'help' => t('Time-based aggregate data about analytics events'),
      ),
    ),

    'time' => array(
      'title' => t('Time'),
      'help' => t('The time for which this aggregate data is collected.'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
    ),

    'period' => array(
      'title' => t('Period'),
      'help' => t('The length of period the data is gathered for (ex. hourly, daily, etc)'),
      'filter' => array(
        'handler' => 'analyticsapi_views_handler_filter_period',
      ),
    ),
  );

  foreach (analyticsapi_get_slice() as $slice_name => $slice) {
    $event = analyticsapi_get_event($info['event']);
    $table = 'analyticsapi_slice_' . $slice_name;
    $slice_title = !empty($slice['title']) ? $slice['title'] : $slice_name;

    $data[$table] = array(
      'table' => array(
        'group' => t('Analytics') . " ($slice_title)",
        'join' => array(
          'analyticsapi_datapoints' => array(
            'left_field' => 'datapoint_id',
            'field'      => 'datapoint_id',
          ),
        ),
      ),
    );

    // put together a master list of the fields
    $fields = $slice['fields'];
    foreach (analyticsapi_get_aggregate() as $aggregate_name => $aggregate) {
      if ($aggregate['slice'] != $slice_name) {
        continue;
      }

      if ($aggregate['function'] == 'count') {
        $fields[$aggregate_name] = array('type' => 'count', 'title' => $aggregate['title']);
      }
      else {
        $fields[$aggregate_name] = $event['fields'][$aggregate['event']];
        $fields[$aggregate_name]['title'] = $aggregate['title'];
      }
    }

    // do the views integration for each field
    foreach ($fields as $field_name => $field) {
      $field_title = !empty($field['title']) ? $field['title'] : $field_name;

      $data[$table][$field_name] = array(
        'title' => $field_title,
        // TODO: is it possible to do help?
      );

      switch ($field['type']) {
        case 'count':
          $data[$table][$field_name] += array(
            'field' => array(
              'handler' => 'views_handler_field_numeric',
              'click sortable' => TRUE,
            ),

            'sort' => array(
              'handler' => 'views_handler_sort',
            ),

            'filter' => array(
              'handler' => 'views_handler_filter_numeric',
            ),
          );
          break;

        case 'nid':
          $data[$table][$field_name] += array(
            'relationship' => array(
              'handler' => 'views_handler_relationship',
              'base' => 'node',
              'base field' => 'nid',
              'label' => $field_title,
            ),

            'argument' => array(
              'handler' => 'views_handler_argument_node_nid',
            ),
          );
          break;

        case 'uid':
          $data[$table][$field_name] += array(
            'relationship' => array(
              'handler' => 'views_handler_relationship',
              'base' => 'users',
              'base field' => 'uid',
              'label' => $field_title,
            ),

            'argument' => array(
              'handler' => 'views_handler_argument_user_uid',
            ),
          );
          break;

        // TODO: we need to handle other types!
      }
    }
  }

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function analyticsapi_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'analyticsapi') . '/views',
    ),
    'handlers' => array(
      'analyticsapi_views_handler_filter_period' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}

